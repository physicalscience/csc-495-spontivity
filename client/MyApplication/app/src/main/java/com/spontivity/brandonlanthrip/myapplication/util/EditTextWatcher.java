package com.spontivity.brandonlanthrip.myapplication.util;

import android.text.InputType;
import android.view.View;
import android.widget.EditText;

/**
 * Created by brandonlanthrip on 3/21/17.
 */

public class EditTextWatcher implements View.OnFocusChangeListener {
    private EditText text;
    private boolean isPassword;
    private String textDefault;

    public EditTextWatcher(EditText text, boolean isPassword, String d) {
        this.text = text;
        this.isPassword = isPassword;
        this.textDefault = d;
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(text.getText().toString().equals(textDefault)) text.getText().clear();
        if(isPassword) text.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }
}
