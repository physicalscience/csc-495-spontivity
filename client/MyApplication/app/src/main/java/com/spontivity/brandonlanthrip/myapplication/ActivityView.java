package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ActivityView extends Activity {

    private int userId;
    private int activityId;
    private String userData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        TextView title = (TextView) findViewById(R.id.activity_title);
        TextView description = (TextView) findViewById(R.id.activity_description);
        TextView time = (TextView) findViewById(R.id.activity_time);
        TextView attend = (TextView) findViewById(R.id.activity_attendee_count);
        Button join = (Button) findViewById(R.id.join_activity);
        ListView comments = (ListView) findViewById(R.id.comments);
        String activity = getIntent().getExtras().getString("activity");
        userId = getIntent().getExtras().getInt("userId");
        userData = getIntent().getExtras().getString("user");

        try {
            JSONObject object = new JSONObject(activity).getJSONObject("activity");
            title.setText(object.getString("name"));
            activityId = object.getInt("id");
            description.setText(object.getString("description"));
            time.setText(object.getString("time"));
            attend.setText(object.getString("userNumber"));
            JSONArray allComments = object.getJSONArray("comments");
            ArrayList<Pair<String, String>> comList = new ArrayList<>();
            for(int i = 0; i < allComments.length(); i++) {
                Pair<String, String> p = new Pair<>();
                JSONObject o = allComments.getJSONObject(i);
                p.a = o.getString("userName");
                p.b = o.getString("content");
                comList.add(p);
            }
            Pair<String, String> post = new Pair<>();
            post.a = "POST_COMMENT";
            comList.add(post);

            CommentAdapter commentAdapter = new CommentAdapter(this, R.layout.comment_content, comList);
            comments.setAdapter(commentAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(getIntent().getExtras().getBoolean("joined")) join.setVisibility(View.INVISIBLE);
        else join.setOnClickListener(new JoinActivityListener(this, join));

        if(getIntent().getExtras().getBoolean("just-joined")) Toast.makeText(this, "You just joined: " + title.getText() + "!", Toast.LENGTH_LONG).show();

    }

    // BROKEN
//    @Override
//    public void onBackPressed() {
//        startActivity(new Intent(this, SponDashBoard.class).putExtra("user", userData));
//    }

    class JoinActivityListener implements View.OnClickListener {
        private Activity owner;
        private Button join;

        public JoinActivityListener(Activity owner, Button join) {
            this.owner = owner;
            this.join = join;
        }

        @Override
        public void onClick(View v) {
            join.setVisibility(View.INVISIBLE);
        }
    }


    class CommentAdapter extends ArrayAdapter<Pair<String, String>> {
        private Context context;
        private List<Pair<String, String>> list;

        public CommentAdapter(Context context, int resource, List<Pair<String, String>> objects) {
            super(context, resource, objects);
            this.context = context;
            this.list = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View rowView;
            if(list.get(position).a.equals("POST_COMMENT")) {
                rowView = inflater.inflate(R.layout.comment_new, parent, false);
                final EditText userComment = (EditText) rowView.findViewById(R.id.posted_comment);
                Button post = (Button) rowView.findViewById(R.id.comment_post_button);
                final int finalUserId = userId;
                Log.e("USER TAG", Integer.toString(userId));
                final int finalActivityId = activityId;
                final String finalUserData = userData;

                post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String commentText = userComment.getText().toString();
                        new AsyncTask<Void, Void, Pair<Boolean, String>>() {
                            @Override
                            protected Pair<Boolean, String> doInBackground(Void... params) {
                                RequestBody body = new FormBody.Builder()
                                        .add("activityId", Integer.toString(activityId))
                                        .add("userId", Integer.toString(userId))
                                        .add("content", commentText)
                                        .build();
                                return new RestHelper().put(body, "/post_comment");
                            }

                            @Override
                            protected void onPostExecute(Pair<Boolean, String> p) {
                                super.onPostExecute(p);
                                // JOINED ON THIS NEEDS TO BE CHANGED BECAUSE THAT IS A BUG RIGHT NOW
                                if(p.a) startActivity(new Intent(context, ActivityView.class).putExtra("activity", p.b).putExtra("userId", finalUserId).putExtra("joined", false).putExtra("user", finalUserData));
                                else Toast.makeText(context, p.b, Toast.LENGTH_LONG).show();

                            }
                        }.execute();
                    }
                });

            } else {
                rowView = inflater.inflate(R.layout.comment_content, parent, false);
                TextView title = (TextView) rowView.findViewById(R.id.comment_title);
                TextView comment = (TextView) rowView.findViewById(R.id.comment_actual_content);

                title.setText(list.get(position).a);
                comment.setText(list.get(position).b);
            }

            return rowView;
        }
    }

}
