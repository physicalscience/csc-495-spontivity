package com.spontivity.brandonlanthrip.myapplication;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;
import com.spontivity.brandonlanthrip.myapplication.util.TriplePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.id;

public class SponDashBoard extends AppCompatActivity {

    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity owner = this;
        setContentView(R.layout.activity_spon_dash_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.dash_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        final ListView activityList = (ListView) findViewById(R.id.activity_list);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        FloatingActionButton brows = (FloatingActionButton) findViewById(R.id.brows_fab);
        Bundle bundle = getIntent().getExtras();

        final String userData = bundle.getString("user");

        try {
            JSONObject user = new JSONObject(userData).getJSONObject("user");
            id = user.getInt("id");
            JSONArray activities = user.getJSONArray("activities");
            if(activities.length() == 0) {
                TextView text = new TextView(this);
                text.setText("No activities joined yet!");
                LinearLayout list = (LinearLayout) findViewById(R.id.activity_linear_layout);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_spon_dash_board);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.CENTER_IN_PARENT);
                text.setLayoutParams(params);
                layout.removeView(list);
                layout.addView(text);
            }
            ArrayList<TriplePair<String, String, String, String, String, String>> actList = new ArrayList<>();
            TriplePair<String, String, String, String, String, String> space = new TriplePair<>();
            space.a = "SPACE";
            actList.add(space);
            for(int i = 0; i < activities.length(); i++) {
                JSONObject o = activities.getJSONObject(i);
                TriplePair<String, String, String, String, String, String> p = new TriplePair<>();
                p.a = o.getString("type");
                p.b = o.getString("name");
                p.c = o.getString("description");
                p.d = o.getString("time");
                p.e = o.getString("userNumber");
                p.f = o.getString("id");
                actList.add(p);
                actList.add(space);
            }
            ActivityArrayAdapter recentActivityAdapter = new ActivityArrayAdapter(owner, R.layout.dash_list_item, actList);
            activityList.setAdapter(recentActivityAdapter);

            Log.e("GOOD", activities.toString());

        } catch (JSONException e) {
            Log.e("AHHHHH", e.toString());
        }

        activityList.setOnItemClickListener(new ActivityClick(this, activityList, id, userData));

        fab.setOnClickListener(new AddActivityClicker(this, id));

        brows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(owner, Brows.class).putExtra("userId", id));
            }
        });

    }

    @Override
    public void onBackPressed() {

    }

    public static class CreateDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final AlertDialog.Builder show = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();

            show.setView(inflater.inflate(R.layout.create_activity_dialog, null));

            return show.create();
        }
    }

    class ActivityArrayAdapter extends ArrayAdapter<TriplePair<String, String, String, String, String, String>> {
        private Context context;
        private List<TriplePair<String, String, String, String, String, String>> activities;

        public ActivityArrayAdapter(Context context, int resource, List<TriplePair<String, String, String, String, String, String>> objects) {
            super(context, resource, objects);
            this.context = context;
            this.activities = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View rowView;
            if(activities.get(position).a.equals("SPACE")) {
                rowView = inflater.inflate(R.layout.content_title, parent, false);
            }
            else {
                rowView = inflater.inflate(R.layout.dash_list_item, parent, false);
                ImageView icon = (ImageView) rowView.findViewById(R.id.activity_icon);
                TextView title = (TextView) rowView.findViewById(R.id.activity_list_title);
                TextView sub = (TextView) rowView.findViewById(R.id.activity_list_sub_title);
                TextView time = (TextView)rowView.findViewById(R.id.time_text);
                TextView attendee = (TextView) rowView.findViewById(R.id.attendee_text);
                TextView act = (TextView) rowView.findViewById(R.id.activity_id);

                switch (activities.get(position).a) {
                    case "SPORT": icon.setImageResource(R.mipmap.sport);
                        break;
                    case "VIDEO_GAME": icon.setImageResource(R.mipmap.video_game);
                        break;
                    case "PARTY": icon.setImageResource(R.mipmap.party);
                        break;
                    case "BOARD_GAME": icon.setImageResource(R.mipmap.board_game);
                        break;
                    default: icon.setImageResource(R.mipmap.default_act);
                        break;
                }

                title.setText(activities.get(position).b);
                sub.setText(activities.get(position).c);
                time.setText(activities.get(position).d);
                attendee.setText(activities.get(position).e);
                act.setText(activities.get(position).f);
            }

            return rowView;
        }
    }

    class ActivityClick implements AdapterView.OnItemClickListener {
        private final Activity owner;
        private final ListView theView;
        private final int userId;
        private final String userData;
        public ActivityClick(Activity owner, ListView theView, int id, String userData) {
            this.owner = owner;
            this.theView = theView;
            this.userId = id;
            this.userData = userData;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final TextView act = (TextView) view.findViewById(R.id.activity_id);
            final String actId = act.getText().toString();
            new AsyncTask<Void, Void, Pair<Boolean, String>>() {

                @Override
                protected Pair<Boolean, String> doInBackground(Void... params) {
                    return new RestHelper().get("/activity?activityId="+actId);
                }

                @Override
                protected void onPostExecute(Pair<Boolean, String> p) {
                    super.onPostExecute(p);
                    if(p.a) owner.startActivity(new Intent(owner, ActivityView.class).putExtra("activity", p.b).putExtra("joined", true).putExtra("userId", userId).putExtra("user", userData));
                    else {
                        Toast.makeText(owner, p.b, Toast.LENGTH_LONG).show();
                    }

                }
            }.execute();
        }
    }

    class AddActivityClicker implements View.OnClickListener {
        private Activity owner;
        private int id;

        public AddActivityClicker(Activity owner, int id) {
            this.owner = owner;
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(owner, NewActivity.class).putExtra("userId", id));
            //CreateDialog dialog = new CreateDialog();
            //dialog.show(getFragmentManager(), "create");
        }
    }

}
