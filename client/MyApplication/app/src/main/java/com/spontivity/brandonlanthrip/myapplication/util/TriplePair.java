package com.spontivity.brandonlanthrip.myapplication.util;

/**
 * Created by brandonlanthrip on 3/25/17.
 */

public class TriplePair<A, B, C, D, E, F> {
    public A a;
    public B b;
    public C c;
    public D d;
    public E e;
    public F f;
}
