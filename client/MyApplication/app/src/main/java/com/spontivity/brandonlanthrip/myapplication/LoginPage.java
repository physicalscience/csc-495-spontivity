package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;

public class LoginPage extends Activity {

    private CallbackManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        manager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id._fb_login);

        loginButton.registerCallback(manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                new Login(LoginPage.this, loginResult.getAccessToken().getToken()).execute();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("THERE IS ERROR", error.getMessage());
                Toast.makeText(LoginPage.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        manager.onActivityResult(requestCode, resultCode, data);
    }

    static class Login extends AsyncTask<Void, Void, Pair<Boolean, String>> {
        private Activity owner;
        private String token;

        public Login(Activity owner, String accessToken) {
            this.owner = owner;
            this.token = accessToken;
        }

        @Override
        protected Pair<Boolean, String> doInBackground(Void... params) {
            Profile p = Profile.getCurrentProfile();
            return new RestHelper().get("/user?accessToken=" + token + "&firstName=" + p.getFirstName() + "&lastName=" + p.getLastName());
        }

        @Override
        protected void onPostExecute(Pair<Boolean, String> s) {
            super.onPostExecute(s);
            if(s.a) owner.startActivity(new Intent(owner, SponDashBoard.class).putExtra("user", s.b).putExtra("tab", "joined"));
            else Toast.makeText(owner, s.b, Toast.LENGTH_LONG).show();
        }
    }
}
