package com.spontivity.brandonlanthrip.myapplication.util;

/**
 * Created by brandonlanthrip on 3/5/17.
 */

public class Pair<A, B> {
    public A a;
    public B b;
}
