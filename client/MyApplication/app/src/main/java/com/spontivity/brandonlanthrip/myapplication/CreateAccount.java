package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.spontivity.brandonlanthrip.myapplication.util.EditTextWatcher;
import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateAccount extends AppCompatActivity {
    EditText firstName;
    EditText lastName;
    EditText email;
    EditText password;
    EditText description;
    private final String D_FIRST = "First Name";
    private final String D_LAST = "Last Name";
    private final String D_EMAIL = "Email";
    private final String D_DESCRIPTION = "Personal Description";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        firstName = (EditText) findViewById(R.id.firstName_field);
        lastName = (EditText) findViewById(R.id.lastName_field);
        email = (EditText) findViewById(R.id.email_field);
        password = (EditText) findViewById(R.id.password_field);
        description = (EditText) findViewById(R.id.personal_description_field);
        Button create = (Button) findViewById(R.id.create_profile_button);

        firstName.setOnFocusChangeListener(new EditTextWatcher(firstName, false, "First Name"));
        lastName.setOnFocusChangeListener(new EditTextWatcher(lastName, false, "Last Name"));
        email.setOnFocusChangeListener(new EditTextWatcher(email, false, "Email"));
        password.setOnFocusChangeListener(new EditTextWatcher(password, true, "Password"));
        description.setOnFocusChangeListener(new EditTextWatcher(description, false, "Personal Description"));

        create.setOnClickListener(new CreatePress(this));
    }

    class CreatePress implements View.OnClickListener {
        Activity owner;

        public CreatePress(Activity owner) {
            this.owner = owner;
        }
        @Override
        public void onClick(View v) {
            if(firstName.getText().toString().equals(D_FIRST)) toastMessage("Please enter a first name!");
            else if(lastName.getText().toString().equals(D_LAST)) toastMessage("Please enter a last name!");
            else if(email.getText().toString().equals(D_EMAIL)) toastMessage("Please enter an email!");
            else if(password.getText().toString() == null) toastMessage("Please enter a password!");
            else if(description.getText().toString().equals(D_DESCRIPTION)) toastMessage("Please enter a description!");
            else {
                new CreateExecute(owner, email.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), password.getText().toString(), description.getText().toString()).execute();
            }
        }

        private void toastMessage(String message) {
            Toast.makeText(owner, message, Toast.LENGTH_LONG).show();
        }
    }

    class CreateExecute extends AsyncTask<Void, Void, Pair<Boolean, String>> {
        private Activity owner;
        private String email, firstName, lastName, password, description;

        public CreateExecute(Activity owner, String email, String firstName, String lastName, String password, String description) {
            this.owner = owner;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.password = password;
            this.description = description;
        }

        @Override
        protected Pair<Boolean, String> doInBackground(Void... params) {
            RestHelper helper = new RestHelper();
            RequestBody body = new FormBody.Builder()
                    .add("email", email)
                    .add("password", password)
                    .add("firstName", firstName)
                    .add("lastName", lastName)
                    .add("description", description)
                    .build();

            return helper.post(body, "/user");
        }

        @Override
        protected void onPostExecute(Pair<Boolean, String> booleanStringPair) {
            super.onPostExecute(booleanStringPair);
            Toast.makeText(owner, booleanStringPair.b, Toast.LENGTH_LONG).show();
            if(booleanStringPair.a) startActivity(new Intent(owner, SponDashBoard.class).putExtra("user", booleanStringPair.b));
            else Toast.makeText(owner, booleanStringPair.b, Toast.LENGTH_LONG).show();
        }
    }



}
