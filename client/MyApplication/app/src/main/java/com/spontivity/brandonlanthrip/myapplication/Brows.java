package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;
import com.spontivity.brandonlanthrip.myapplication.util.TriplePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Brows extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browes);
        ListView list = (ListView) findViewById(R.id.brows_activity_list);
        int userId = getIntent().getExtras().getInt("userId");

        list.setOnItemClickListener(new ActivityClick(this, list, userId));

        new GrabRecent(this, list, new ArrayList<TriplePair<String, String, String, String, String, String>>(), userId).execute();
    }

    class GrabRecent extends AsyncTask<Void, Void, Pair<Boolean, String>> {
        private Activity owner;
        private ListView recentActivities;
        private ArrayList<TriplePair<String, String, String, String, String, String>> list;
        private int userId;

        public GrabRecent(Activity owner, ListView recentActivities, ArrayList<TriplePair<String, String, String, String, String, String>> list,  int userId) {
            this.owner = owner;
            this.recentActivities = recentActivities;
            this.userId = userId;
            this.list = list;
        }

        @Override
        protected Pair<Boolean, String> doInBackground(Void... params) {
            RestHelper helper = new RestHelper();
            return helper.get("/activity_get");
        }

        @Override
        protected void onPostExecute(Pair<Boolean, String> response) {
            super.onPostExecute(response);
            if(response.a) {
                try {
                    JSONObject acts = new JSONObject(response.b);
                    JSONArray allActs = acts.getJSONArray("recentActivities");
                    TriplePair<String, String, String, String, String, String> space = new TriplePair<>();
                    space.a = "SPACE";
                    list.add(space);
                    //DEBUG
                    Random r = new Random();
                    for (int i = 0; i < allActs.length(); i++) {
                        JSONObject o = allActs.getJSONObject(i);
                        TriplePair<String, String, String, String, String, String> p = new TriplePair<>();
                        if (o.getLong("ownerId") != userId) {
                            p.a = o.getString("type");
                            p.b = o.getString("name");
                            p.c = o.getString("description");
                            p.d = o.getString("time");
                            //DEBUG
                            p.e = Integer.toString(r.nextInt(30));
                            p.f = o.getString("id");
                            list.add(p);
                            list.add(space);
                        }
                    }
                    ActivityArrayAdapter recentActivityAdapter = new ActivityArrayAdapter(owner, R.layout.dash_list_item, list);
                    recentActivities.setAdapter(recentActivityAdapter);
                }catch(JSONException e) {

                }
            }
        }
    }

    class ActivityArrayAdapter extends ArrayAdapter<TriplePair<String, String, String, String, String, String>> {
        private Context context;
        private List<TriplePair<String, String, String, String, String, String>> activities;

        public ActivityArrayAdapter(Context context, int resource, List<TriplePair<String, String, String, String, String, String>> objects) {
            super(context, resource, objects);
            this.context = context;
            this.activities = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View rowView;
            if(activities.get(position).a.equals("SPACE")) {
                rowView = inflater.inflate(R.layout.content_title, parent, false);
            }
            else {
                rowView = inflater.inflate(R.layout.dash_list_item, parent, false);
                ImageView icon = (ImageView) rowView.findViewById(R.id.activity_icon);
                TextView title = (TextView) rowView.findViewById(R.id.activity_list_title);
                TextView sub = (TextView) rowView.findViewById(R.id.activity_list_sub_title);
                TextView time = (TextView)rowView.findViewById(R.id.time_text);
                TextView attendee = (TextView) rowView.findViewById(R.id.attendee_text);
                TextView act = (TextView) rowView.findViewById(R.id.activity_id);

                switch (activities.get(position).a) {
                    case "SPORT": icon.setImageResource(R.mipmap.sport);
                        break;
                    case "VIDEO_GAME": icon.setImageResource(R.mipmap.video_game);
                        break;
                    case "PARTY": icon.setImageResource(R.mipmap.party);
                        break;
                    case "BOARD_GAME": icon.setImageResource(R.mipmap.board_game);
                        break;
                    default: icon.setImageResource(R.mipmap.default_act);
                        break;
                }

                title.setText(activities.get(position).b);
                sub.setText(activities.get(position).c);
                time.setText(activities.get(position).d);
                attendee.setText(activities.get(position).e);
                act.setText(activities.get(position).f);
            }

            return rowView;
        }
    }

    class ActivityClick implements AdapterView.OnItemClickListener {
        private final Activity owner;
        private final ListView theView;
        private final int userId;

        public ActivityClick(Activity owner, ListView theView, int userId) {
            this.owner = owner;
            this.theView = theView;
            this.userId = userId;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final TextView act = (TextView) view.findViewById(R.id.activity_id);
            final String actId = act.getText().toString();
            new AsyncTask<Void, Void, Pair<Boolean, String>>() {

                @Override
                protected Pair<Boolean, String> doInBackground(Void... params) {
                    return new RestHelper().get("/activity?activityId="+actId);
                }

                @Override
                protected void onPostExecute(Pair<Boolean, String> p) {
                    super.onPostExecute(p);
                    if(p.a) owner.startActivity(new Intent(owner, ActivityView.class).putExtra("activity", p.b).putExtra("joined", false).putExtra("userId", userId));
                    else {
                        Toast.makeText(owner, p.b, Toast.LENGTH_LONG).show();
                    }

                }
            }.execute();
        }
    }
}
