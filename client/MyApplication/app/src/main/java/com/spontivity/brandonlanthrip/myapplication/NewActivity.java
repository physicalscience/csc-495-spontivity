package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.spontivity.brandonlanthrip.myapplication.util.Pair;
import com.spontivity.brandonlanthrip.myapplication.util.RestHelper;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class NewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        final Bundle bundle = getIntent().getExtras();
        final Activity owner = this;
        Button timeButton = (Button) findViewById(R.id.time_button);
        Button typeButton = (Button) findViewById(R.id.type_button);
        Button create = (Button) findViewById(R.id.activity_create_button);
        Button cancel = (Button) findViewById(R.id.activity_create_cancel_button);
        final EditText activityName = (EditText) findViewById(R.id.activity_name_edit);
        final EditText descriptionName = (EditText) findViewById(R.id.description_input);
        final TextView timeText = (TextView) findViewById(R.id.time_display);
        final TextView typeText = (TextView) findViewById(R.id.chosen_type_text);

        typeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TypePicker picker = new TypePicker();
                picker.show(getFragmentManager(), "type");
            }
        });

        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePicker picker = new TimePicker();
                picker.show(getFragmentManager(), "time");
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String typeCode;
                switch (typeText.getText().toString()) {
                    case "Sport": typeCode = "SPORT";
                        break;
                    case "Party": typeCode = "PARTY";
                        break;
                    case "Video Game": typeCode = "VIDEO_GAME";
                        break;
                    case "Board Game": typeCode = "BOARD_GAME";
                        break;
                    default: typeCode = "DEFAULT";

                }
                new CreateActivity(owner).execute(Integer.toString(bundle.getInt("userId")), activityName.getText().toString(),typeCode, descriptionName.getText().toString(), timeText.getText().toString());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static class TimePicker extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final TextView timeView = (TextView) getActivity().findViewById(R.id.time_display);
            final AlertDialog.Builder show = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();

            show.setView(inflater.inflate(R.layout.time_select, null))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            android.widget.TimePicker picker = (android.widget.TimePicker) ((AlertDialog) dialog).findViewById(R.id.timePicker);
                            int hour = picker.getHour();
                            if(hour > 12) {
                                hour -= 12;
                                if(hour < 10) {
                                    timeView.setText(" " + hour + ":" + picker.getMinute() + " PM");
                                }else {
                                    timeView.setText(hour + ":" + picker.getMinute() + " PM");
                                }
                            }else {
                                if(hour < 10) {
                                    timeView.setText(" " + hour + ":" + picker.getMinute() + " AM");
                                }
                                else {
                                    timeView.setText(hour + ":" + picker.getMinute() + " AM");
                                }
                            }

                            timeView.setTextSize(24);

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TimePicker.this.getDialog().cancel();
                        }
                    });
            return show.create();
        }
    }

    public static class TypePicker extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final TextView typeView = (TextView) getActivity().findViewById(R.id.chosen_type_text);
            final AlertDialog.Builder show = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();

            show.setView(inflater.inflate(R.layout.type_picker, null))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            RadioGroup group = (RadioGroup) ((AlertDialog) dialog).findViewById(R.id.type_group);
                            switch (group.getCheckedRadioButtonId()) {
                                case R.id.sport_radio: typeView.setText("Sport");
                                    break;
                                case R.id.party_radio: typeView.setText("Party");
                                    break;
                                case R.id.video_game_radio: typeView.setText("Video Game");
                                    break;
                                case R.id.board_game_radio: typeView.setText("Board Game");
                                    break;
                            }
                            typeView.setTextSize(24);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            TypePicker.this.getDialog().cancel();
                        }
                    });
            return show.create();
        }
    }

    class CreateActivity extends AsyncTask<String, Void, Pair<Boolean, String>> {
        Activity owner;

        public CreateActivity(Activity owner) {
            this.owner = owner;
        }
        @Override
        protected Pair<Boolean, String> doInBackground(String... params) {
            RestHelper helper = new RestHelper();
            RequestBody body = new FormBody.Builder()
                    .add("ownerId", params[0])
                    .add("name", params[1])
                    .add("typeCode", params[2])
                    .add("description", params[3])
                    .add("time", params[4])
                    .build();
            return helper.post(body, "/activity");
        }

        @Override
        protected void onPostExecute(Pair<Boolean, String> p) {
            super.onPostExecute(p);
            if(p.a) {
                startActivity(new Intent(owner, SponDashBoard.class).putExtra("user", p.b).putExtra("tab", "joined"));
            } else {
                Toast.makeText(owner, p.b, Toast.LENGTH_LONG).show();
            }
        }
    }
}
