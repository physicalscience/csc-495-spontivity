package com.spontivity.brandonlanthrip.myapplication.util;

import android.util.Log;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by brandonlanthrip on 3/8/17.
 */

public class RestHelper {
    private static final String BASE_URL = "http://pi.cs.oswego.edu:22340";
    private OkHttpClient client = new OkHttpClient();

    public Pair<Boolean, String> get(String urlExtension) {
        Request request = new Request.Builder()
                .url(BASE_URL + urlExtension)
                .build();
        return makeRequest(request);
    }

    public Pair<Boolean, String> post(RequestBody body, String urlExtension) {
        Request request = new Request.Builder()
                .url(BASE_URL + urlExtension)
                .post(body)
                .build();


        Log.e("IN REQUEST", request.toString());
        return makeRequest(request);
    }

    public Pair<Boolean, String> put(RequestBody body, String urlExtension) {
        Request request = new Request.Builder()
                .url(BASE_URL + urlExtension)
                .put(body)
                .build();
        return makeRequest(request);
    }

    private Pair<Boolean, String> makeRequest(Request request) {
        Pair<Boolean, String> pair = new Pair<>();
        try {
            Response response = client.newCall(request).execute();
            int code = response.code();
            pair.b = response.body().string();

            if(code == 200) pair.a = true;
            else pair.a = false;

            return pair;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pair;
    }
}
