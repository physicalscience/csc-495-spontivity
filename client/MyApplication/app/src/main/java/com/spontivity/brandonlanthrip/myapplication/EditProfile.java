package com.spontivity.brandonlanthrip.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class EditProfile extends Activity {
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Button saveButton = (Button) findViewById(R.id.save_edit_changes);
        Button cancel = (Button) findViewById(R.id.cancel_edit_changes);
        EditText firstName = (EditText) findViewById(R.id.firstName);
        EditText lastName = (EditText) findViewById(R.id.lastName);
        EditText password = (EditText) findViewById(R.id.password);
        EditText description = (EditText) findViewById(R.id.description);
        bundle = getIntent().getExtras();
        try {
            JSONObject json = new JSONObject(bundle.getString("user")).getJSONObject("user");
            firstName.setText(json.getString("firstName"));
            lastName.setText(json.getString("lastName"));
            password.setText(json.getString("password"));
            description.setText(json.getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        saveButton.setOnClickListener(new SaveClick(this));
        cancel.setOnClickListener(new CancelClick(this));
    }

    class SaveClick implements View.OnClickListener {
        private Activity owner;
        public SaveClick(Activity owner) {
            this.owner = owner;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(owner, SponDashBoard.class).putExtra("user", bundle.getString("user")));
        }
    }

    class CancelClick implements View.OnClickListener {
        private Activity owner;
        public CancelClick(Activity owner) {
            this.owner = owner;
        }

        @Override
        public void onClick(View v) {
            startActivity(new Intent(owner, SponDashBoard.class).putExtra("user", bundle.getString("user")));
        }
    }
}
