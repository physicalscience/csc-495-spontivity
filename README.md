# Spontivity
Spontivity is my capstone project for CSC 495 at SUNY Oswego.  
  
## What is Spontivity?
Simply put, Spontivity is a selfish social media app for finding  
people to hang out and do activities that would basically be  
impossible with only one person (ie: Tennis or a pick up game of basketball).  

## Tools

### Backend
I am using Akka http with Slick to handle the webservice for this project

### Frontend
This application will be avaiable for Android phones with Android 6 and above.  