package backend

class Comment {
    String content
    User owner

    static belongsTo = [activity: Activity]
    static constraints = {
        content blank: false
    }
}
