package backend

class AuthToken {
    String accessToken

    static belongsTo = [user: User]

    static constraints = {
    }
}
