package backend

import com.spontivity.utils.ActivityType


class Activity {
    String name
    ActivityType type
    String description
    String time
    int ownerId
    static hasMany = [participants: User, comments: Comment]
    static constraints = {
        name blank: false, size: 4..20
        type nullable: false
        description blank: false, size: 4..250
        participants nullable: true
        comments nullable: true
        time blank: false
    }
}
