package backend


import grails.rest.*
@Resource(uri = "/user", formats = ['json', 'xml'])
class User {
    String firstName
    String lastName
    String description

    static hasMany = [comments: Comment, activities: Activity]
    static belongsTo = Activity
    static hasOne = [token: AuthToken]
    static constraints = {
        firstName blank: false, size: 4..20
        lastName blank: false, size: 4..20
        description nullable: true, size: 0..255
        comments nullable: true
        activities nullable: true
    }
}