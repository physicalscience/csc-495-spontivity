package backend

class UrlMappings {

    static mappings = {

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')

        "/user"(controller: 'user', action: 'showOrCreate', method: 'GET')
        "/user"(controller: 'user', action: 'post', method: 'POST')

        "/activity"(controller: 'activity', action: 'post', method: 'POST')
        "/activity"(controller: 'activity', action: 'put', method: 'PUT')
        "/activity"(controller: 'activity', action: 'get', method: 'GET')
        "/activity_search"(controller: 'activity', action: 'search', method: 'GET')
        "/activity_get"(controller: 'activity', action: 'grab', method: 'GET')
        "/post_comment"(controller: 'activity', action: 'postComment', method: 'PUT')

    }
}
