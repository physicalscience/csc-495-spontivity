package backend

import com.spontivity.utils.ActivityType

class ActivityController {
	static responseFormats = ['json', 'xml']
    PreconditionService preconditionService
    ActivityService activityService
	
    def index() { }

    /**
     * Create a new Activity
     * @param ownerId - the id of the user creating the activity
     * @param name - the name of the activity
     * @param typeCode - the type of the activity
     * @param description - a description of the activity
     * @param time - the time the activity is going to happen
     * @return - returns a JSON view
     */
    def post(long ownerId, String name, String typeCode, String description, String time) {
        def precondition = preconditionService.checkPreconditions(params, ["name", "ownerId", "typeCode", "description", "time"])

        if(precondition) {
            def owner = activityService.findUser(ownerId)
            def type = activityService.findActivityByCode(typeCode)
            if(owner && type ) {
                def activity = new Activity(name: name, type: type, description: description, ownerId: ownerId, time: time)
                activity.addToParticipants(owner)
                owner.addToActivities(activity)
                owner.save(flush: true)
                activity.save(flush: true, failOnError: true)
                render(view: 'returnUser', model: [user: owner])
            }
        }
    }

    /**
     * Add a user to an Activity
     * @param userId - the id of the user to be added
     * @param activityId - the id of the activity
     * @return - returns a JSON view
     */
    def put(long userId, long activityId) {
        def precondition = preconditionService.checkPreconditions(params, ["userId", "activityId"])

        if(precondition) {
            def user = activityService.findUser(userId)
            def activity = activityService.findActivityById(activityId)
            if(user && activity) {
                activity.addToParticipants(user)
                user.addToActivities(activity)
                activity.save(flush: true, failOnError: true)
                render(view: 'show', model: [activity: activity])
            }
        }
    }

    /**
     * Gets an activity
     * @param activityId - the activity id
     * @return - returns a JSON view
     */
    def get(long activityId) {
        def precondition = preconditionService.checkPreconditions(params, ["activityId"])

        if(precondition) {
            def activity = activityService.findActivityById(activityId)
            if(activity != null) {
                render(view: 'show', model: [activity: activity])
            }
        }
    }

    /**
     * Searches for activities of a certain type
     * @param activityTypeCode - the type code to search for
     * @return - returns a JSON view
     */
    def search(String activityTypeCode) {
        def precondition = preconditionService.checkPreconditions(params, ["activityTypeCode"])

        if(precondition) {
            def type = ActivityType.getType(activityTypeCode)
            if(type != null) {
                def activities = Activity.findAllByType(type)
                render(view: 'search', model: [activities: activities])
            }
        }
    }

    /**
     * Grabs all activities
     * @return - returns a JSON view
     */
    def grab() {
        render(view: 'grab', model: [activities: Activity.all])
    }

    /**
     * Posts a comment to an activity
     * @param activityId - the id of the activity
     * @param userId - the id of the user
     * @param content - the content of the comment
     * @return - returns a JSON view
     */
    def postComment(long activityId, long userId, String content) {
        def precondition = preconditionService.checkPreconditions(params, ["activityId", "userId", "content"])

        if(precondition) {
            def activity = activityService.findActivityById(activityId)
            def user = activityService.findUser(userId)
            if(user != null && activity != null) {
                def comment = new Comment(owner: user, content: content, activity: activity)
                comment.save(flush: true, failOnError: true)
                activity.addToComments(comment)
                activity.save(flush: true, failOnError: true)
                render(view: 'show', model: [activity: activity])
            } else {
                println("A USER OR ACTIVITY WAS NULL")
            }
        }
    }
}
