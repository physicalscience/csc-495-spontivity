package backend


import grails.rest.*
import grails.converters.*

class UserController extends RestfulController{
	static responseFormats = ['json', 'xml']
    PreconditionService preconditionService

    UserController() {
        super(User)
    }

    /**
     * Returns the user's data or creates a new user if the authToken is not found
     * @param accessToken - the accessToken provided from the facebook api
     * @param firstName - the first name of the user
     * @param lastName - the last name of the user
     * @return - renders the user's data as a json object
     */
    def showOrCreate(String accessToken, String firstName, String lastName) {
        def check = preconditionService.checkPreconditions(params, ["accessToken", "firstName", "lastName"])
        if(check) {
            def token = AuthToken.findByAccessToken(accessToken)
            if(token) {
                User user = token.user
                if(user) {
                    render (view: 'show', model: [user: user])
                } else {
                    render(view: '../failure/failed', model: [message: "cannot find user", code: 400])
                }
            } else {
                def newUser = new User(firstName: firstName, lastName: lastName)
                newUser.setToken(new AuthToken(accessToken: accessToken))
                newUser.save(flush: true, failOnError: true)
                render(view: 'show', model: [user: newUser])
            }
        } else {
            render(view: "../failure/failed", model: [message: "missing params", code: 400])
        }
    }

    /**
     * Updates the user's description
     * @param accessToken - the accessToken of the user provided by facebook
     * @param description - the description the user has entered
     * @return - renders a view of the user with the new description
     */
    def post(String accessToken, String description) {
        def check = preconditionService.checkPreconditions(params, ["accessToken", "description"])
        if(check) {
            def token = AuthToken.findByAccessToken(accessToken)
            if(token) {
                def user = token.user
                if(user) {
                    user.description = description
                    render(view: 'show', model: [user: user])
                }
            } else {
                render(view: '../failure/failed', model: [message: "could not find user", code: 400])
            }
        } else {
            render(view: '../failure/failed', model: [message: "missing params", code: 400])
        }
    }
}
