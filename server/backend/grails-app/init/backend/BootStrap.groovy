package backend

import com.spontivity.utils.ActivityType

class BootStrap {

    def init = { servletContext ->
        def brandon = new User(firstName: "Clara", lastName: "Lanthrip")
        def shantelle = new User(firstName: "Shantelle", lastName: "Lanthrip")
        brandon.setToken(new AuthToken(accessToken: "brad"))
        shantelle.setToken(new AuthToken(accessToken: "shant"))
        brandon.save(flush: true)
        shantelle.save(flush: true)
        def border = new Activity(ownerId: 1, name: "BorderLands 2", type: ActivityType.VideoGame, description: "Lets do some raids!", time: " 6:00 PM")
        new Activity(ownerId: 1, name: "Catan", type: ActivityType.BoardGame, description: "Hey lets play some catan", time: "10:30 PM").save(flush: true)
        new Activity(ownerId: 1, name: "Soccer", type: ActivityType.Sport, description: "I am playing some soccer in the park!", time: "12:00 PM").save(flush: true)
        new Activity(ownerId: 1, name: "Pool", type: ActivityType.Party, description: "Pool tournament at Greene's!", time: "11:00 PM").save(flush: true)
        new Activity(ownerId: 2, name: "House Party", type: ActivityType.Party, description: "Throwing a Party at my house today", time: " 3:00 PM").save(flush: true)
        new Activity(ownerId: 2, name: "BasketBall", type: ActivityType.Sport, description: "Playing basketball in the gym", time: "10:30 AM").save(flush: true)
        new Activity(ownerId: 2, name: "Monopoly", type: ActivityType.BoardGame, description: "Anyone want to play some monopoly?", time: " 3:00 PM").save(flush: true)
        new Activity(ownerId: 2, name: "Harry Potter", type: ActivityType.Party, description: "Throwing a giant Harry Potter marathon", time: "2:30 PM").save(flush: true)
        new Activity(ownerId: 2, name: "Star Craft 2", type: ActivityType.VideoGame, description: "Crushing it in star craft 2", time: " 4:00 PM").save(flush: true)
        new Activity(ownerId: 2, name: "DOTA tourney", type: ActivityType.VideoGame, description: "Having a DOTA tourney in the campus center", time: "7:30 PM").save(flush: true)
        new Activity(ownerId: 2, name: "Bar Hop!", type: ActivityType.Party, description: "DOING ALL THE BARS WOO", time: " 5:00 PM").save(flush: true)
        new Activity(ownerId: 2, name: "Fishing", type: ActivityType.Sport, description: "fishing over on the lake!", time: "8:30 AM").save(flush: true)
        new Activity(ownerId: 2, name: "Hockey", type: ActivityType.Sport, description: "Playing a pick up game at the campus center", time: " 10:30 PM").save(flush: true)
        new Activity(ownerId: 2, name: "star trek", type: ActivityType.BoardGame, description: "Playing star trek ascension", time: "11:30 AM").save(flush: true)
        border.save(flush: true, failOnError: true)
        new Comment(owner: brandon, content: "So is everyOne good?", activity: border).save(flush: true)
        new Comment(owner: shantelle, content: "I am still game!", activity: border).save(flush: true)
    }
    def destroy = {
    }
}
