package backend

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class PreconditionService {

    boolean checkPreconditions(GrailsParameterMap map, List<String> parameters) {
        for(String s in parameters) {
            if(!map.containsKey(s)) false
        }
        true
    }
}
