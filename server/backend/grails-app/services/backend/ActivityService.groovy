package backend

import com.spontivity.utils.ActivityType
import grails.transaction.Transactional

@Transactional
class ActivityService {

    User findUser(long userId) {
        User.findById(userId)
    }

    ActivityType findActivityByCode(String code) {
        switch (code) {
            case "VIDEO_GAME": ActivityType.VideoGame
                break
            case "BOARD_GAME": ActivityType.BoardGame
                break
            case "SPORT": ActivityType.Sport
                break
            case "PARTY": ActivityType.Party
                break
            default: null
        }
    }

    Activity findActivityById(long id) { Activity.findById(id) }
}
