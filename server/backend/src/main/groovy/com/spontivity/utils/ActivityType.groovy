package com.spontivity.utils

enum ActivityType {
    VideoGame("VIDEO_GAME"),
    BoardGame("BOARD_GAME"),
    Sport("SPORT"),
    Party("PARTY")

    String code

    ActivityType(String code) {
        this.code = code
    }

    String getCode() {
        code
    }

    static ActivityType getType(String code) {
        switch (code) {
            case "VIDEO_GAME": VideoGame
                break
            case "BOARD_GAME": BoardGame
                break
            case "SPORT": Sport
                break
            case "PARTY": Party
                break
            default: null
        }
    }
}